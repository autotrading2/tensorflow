import pandas as pd

class Repository:

    def __init__(self, path) -> None:
        self.pathFile = path
        pass

    def loadCSV(self):
        self.Stock = pd.DataFrame(pd.read_csv(self.pathFile, sep="\t"))
        self.data = self.Stock.sort_index(ascending=True, axis=0)
        self.new_data = pd.DataFrame(index=range(0,len(self.Stock)),columns=['Close'])

        for i in range(0,len(self.data)):
            # self.new_data['Open'][i] = self.data['<OPEN>'][i]
            # self.new_data['High'][i] = self.data['<HIGH>'][i]
            # self.new_data['Low'][i] = self.data['<LOW>'][i]
            self.new_data['Close'][i] = self.data['<CLOSE>'][i]

        pass
