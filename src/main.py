
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM

# # 1.04


# Stock = pd.DataFrame([5.15,8.97,1.31,1.18,1.50,3.91,3.62,1.47,1.89,8.50,1.48,5.12,11.57,1.09,1.47,2.82,4.35,1.20,1.32,2.26,2.17,1.88,1.68,1.03,1.24,1.42,4.22,31.20,1.31,2.39,1.57,2.11,10.34,1.24,3.02,1.10,1.65,8.38,1.70,1.02,1.47,1.40,1.51,.74,13.46,3.29,2.15,1.60,3.78,1.18,3.97,154.08,44.22,1.09,116.60,1.15,1.37,2.87,4.03,5.07,1.80,4.71,1.23,2.95,1.51,5.17,5.29,1.42,1.45,1.11,15.14,2.31,1,1.73,1.91,1.31,1.05,111.02,5.6,2.18,1.35,2.63,1.07,9.97,4.83,1.19,1.04,2.04,2.82,1.18,3.71,1.73,1.76,1.46,3.17,8.81,1.26,1.28,1.29,1.01,4.48,1.87,1.18,1.13,5.45,3.01,1.43,2.91,1,1.66,1.64,5.36,1,1.07,2.59,23.57,1,1.91,1.04,2.86,1.04,3.23,1.23,2.95,1.51,5.17,5.29,1.42,1.45,1.11,15.14,2.31,1,1.73,1.91,1.31,1.05,111.02,5.6,2.18,1.35,2.63,1.07,9.97,4.83,1.19,1.04,2.04,2.82,1.18,3.71,1.73,1.76,1.46,3.17,8.81,1.26,1.28,1.29,1.01,4.48,1.87,1.18,1.13,5.45,3.01,1.43,2.91,1,1.66,1.64,5.36,1,1.07,2.59,23.57,1,1.91,1.04,2.86,1.04,3.23])


# #Criando o Dataframe
# data = Stock.sort_index(ascending=True, axis=0)
# new_data = pd.DataFrame(index=range(0,len(Stock)),columns=['CLOSE'])


# for i in range(0,len(data[0])):
#     new_data['CLOSE'][i] = data[0][i]

# predictLen = 5
# datasetLen = len(new_data) - predictLen

# #Criando o train e o test set
# dataset = new_data.values
# train = dataset[0:datasetLen,:]
# valid = dataset[datasetLen:,:]

# scaler = MinMaxScaler(feature_range=(0, 1))
# scaled_data = scaler.fit_transform(dataset)

# x_train, y_train = [], []

# for i in range(predictLen,len(train)):
#     x_train.append(scaled_data[i-predictLen:i,0])
#     y_train.append(scaled_data[i,0])

# x_train, y_train = np.array(x_train), np.array(y_train)
# x_train = np.reshape(x_train, (x_train.shape[0],x_train.shape[1],1))

# Criando o modelo LSTM
model = Sequential()
model.add(LSTM(units=predictLen, return_sequences=True, input_shape=(x_train.shape[1],1)))
model.add(LSTM(units=predictLen))
model.compile(loss='mean_squared_error', optimizer='adam')
model.fit(x_train, y_train, epochs=2, batch_size=1, verbose=2)

#Prevendo os 143 últimos preços de ação, baseado nos 5 últimos.
inputs = new_data[len(new_data) - len(valid) - predictLen:].values
inputs = inputs.reshape(-1,1)
inputs  = scaler.transform(inputs)

X_test = []

for i in range(predictLen,inputs.shape[0]):
    X_test.append(inputs[i-predictLen:i,0])

X_test = np.array(X_test)
X_test = np.reshape(X_test, (X_test.shape[0],X_test.shape[1],1))

closing_price = model.predict(X_test)
closing_price = scaler.inverse_transform(closing_price)

print(closing_price[0])