import repository
import numpy as np
from sklearn.preprocessing import MinMaxScaler

class PrepareData :

    def __init__(self, fileName) -> None:
        self.filename = fileName
        pass

    def execute(self, trainLen) :
        data = repository.Repository(self.filename)
        data.loadCSV()

        datasetLen = len(data.new_data) - trainLen
        self.new_data = data.new_data

        #Criando o train e o test set
        dataset = self.new_data.values
        train = dataset[0:datasetLen,:]
        self.valid = dataset[datasetLen:,:]

        self.scaler = MinMaxScaler(feature_range=(0, 1))
        self.scaled_data = self.scaler.fit_transform(dataset)

        self.x_train, self.y_train = [], []

        for i in range(trainLen,len(train)):
            self.x_train.append(self.scaled_data[i-trainLen:i,0])
            self.y_train.append(self.scaled_data[i,0])

        self.x_train, self.y_train = np.array(self.x_train), np.array(self.y_train)
        self.x_train = np.reshape(self.x_train, (self.x_train.shape[0],self.x_train.shape[1],1))
        pass
