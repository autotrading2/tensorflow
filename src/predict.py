import pandas as pd
import prepareData
import numpy as np
import tensorflow as tf

class Predict :

    def __init__(self) -> None:
        self.model = tf.keras.models.load_model('forex')
        self.X_test = []
        self.dataToPredict = []

    def execute(self):
        
        trainLen = 120 
        
        self.preparedData = prepareData.PrepareData('XAUUSD_PREDICT.csv')
        self.preparedData.execute(trainLen)

        inputs = self.preparedData.new_data[len(self.preparedData.new_data) - len(self.preparedData.valid) - trainLen:].values
        inputs = inputs.reshape(-1,1)
        inputs  = self.preparedData.scaler.transform(inputs)

        for i in range(trainLen,inputs.shape[0]):
            self.X_test.append(inputs[i-trainLen:i,0])

        self.dataToPredict = self.X_test
        self.X_test = np.array(self.X_test)
        self.X_test = np.reshape(self.X_test, (self.X_test.shape[0],self.X_test.shape[1],1))

        result = self.preparedData.scaler.inverse_transform(self.model.predict(self.X_test))
        self.reversed_result = result[::-1]

