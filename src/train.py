import numpy as np
import prepareData
from keras.models import Sequential
from keras.layers import Dropout, LSTM, Dense

class Train :

    def execute (self):

        preparedData = prepareData.PrepareData('XAUUSD.csv')
        trainLen = 120
        preparedData.execute(trainLen)

        model = Sequential()
        model.add(LSTM(units=500, return_sequences=True, dropout=0.1, input_shape=(preparedData.x_train.shape[1],1)))
        model.add(LSTM(units=200, return_sequences=True, dropout=0.1))
        model.add(LSTM(units=20))
        model.add(Dense(units=1))
        model.compile(loss='mean_absolute_error', optimizer='adam')
        model.fit(preparedData.x_train, preparedData.y_train, epochs=10, batch_size=32, verbose=1)
        model.save('forex')


train = Train()
train.execute()